#include "Vector.h"

Vector::Vector(const Value* rawArray, const size_t size, float coef)
{
	this->_data = new Value[size];
	for ( size_t i = 0; i < size; ++i )
	{
		this->_data[i] = rawArray[i];
	}
	this->_size = size;
	this->_capacity = size;
	this->_multiplicativeCoef = coef;
}

Vector::~Vector()
{
	delete[] this->_data;
	this->_size = 0;
	this->_capacity = 0;
}

Vector::Vector(const Vector& other)
{
	if ( this->_data != other._data )
	{
		this->_data = new Value[other._size];
		this->_capacity = other._size;
		this->_size = other._size;
		for ( size_t i = 0; i < this->_size; ++i )
		{
			this->_data[i] = other._data[i];
		}
	}
}

Vector& Vector::operator=(const Vector& other)
{
	if ( this->_data != other._data )
	{
		delete [] this->_data;
		this->_capacity = other._size;
		this->_size = other._size;
		this->_data = new Value[other._size];
		for ( size_t i = 0; i < _size; ++i )
		{
			this->_data[i] = other._data[i];
		}
	}
	return *this;
}

Vector::Vector(Vector&& other) noexcept
{
	if ( this->_data != other._data )
	{
		this->_size = other._size;
		this->_capacity = other._size;
		this->_data = other._data;
		other._data = nullptr;
		other._capacity = 0;
		other._size = 0;
	}
}

Vector& Vector::operator=(Vector&& other) noexcept
{
	if ( this->_data != other._data )
	{
		delete [] this->_data;
		this->_size = other._size;
		this->_capacity = other._size;
		this->_data = other._data;
		other._data = nullptr;
		other._capacity = 0;
		other._size = 0;
	}
	return *this;
}

void Vector::pushBack(const Value& value)
{
	if ( this->_size == this->_capacity )
	{
		if ( this->_capacity == 0 )
		{
			this->_capacity = 1 * _multiplicativeCoef;
			Value* newData = new Value[this->_capacity];
			newData[this->_size] = value;
			this->_size += 1;
			this->_data = newData;
		}
		else
		{
			this->_capacity *= _multiplicativeCoef;
			Value* newData = new Value[this->_capacity];
			for ( size_t i = 0; i < this->_size; ++i )
			{
				newData[i] = this->_data[i];
			}
			newData[this->_size] = value;
			this->_size += 1;
			this->_data = newData;
		}
	}
	else
	{
		this->_data[this->_size] = value;
		this->_size += 1;
	}

}

Value& Vector::operator[](size_t idx)
{ 
	if ( (idx < this->_size) && (idx > 0) )
	{
		return this->_data[idx];
	}	
	return this->_data[0];
}

const Value& Vector::operator[](size_t idx) const
{
	if ( (idx < this->_size) && (idx > 0) )
	{
		return this->_data[idx];
	}
	return this->_data[0];
}

size_t Vector::size() const
{
	return this->_size;
}

size_t Vector::capacity() const
{
	return this->_capacity;
}

void Vector::shrinkToFit()
{
	if ( this->_capacity > this->_size )
	{
		Value* newData = new Value[this->_size];
		for ( size_t i = 0; i < this->_size; ++i )
		{
			newData[i] = this->_data[i];
		}
		this->_capacity = this->_size;
		this->_data = newData;	
	}
}

double Vector::loadFactor() const
{
	return this->_size/this->_capacity;
}

long long Vector::find(const Value& value) const
{
	for ( size_t i = 0; i < this->_size; ++i )
	{
		if ( this->_data[i] == value )
		{
			return i;
		}
	}
	return -1;
}

void Vector::reserve(size_t capacity)
{
	if ( capacity > this->_capacity )
	{
		Value* newData = new Value[capacity];
		for ( size_t i = 0; i < this->_size; ++i )
		{
			newData[i] = this->_data[i];
		}
		this->_capacity = capacity;
		this->_data = newData;
	}
}

void Vector::popBack()
{
	if ( this->_size == 0 )
	{
		throw this->_size;
	}
	this->_size--;
}

void Vector::popFront()
{
	if ( this->_size == 0 )
	{
		throw this->_size;
	}
	Value* newData = new Value[this->_capacity];
	this->_size--;
	for ( size_t i = 0; i < this->_size; ++i )
	{
		newData[i] = this->_data[i + 1];
		
	}
	this->_data = newData;

}

void Vector::pushFront(const Value& value)
{
	if ( this->_size == this->_capacity )
	{
		if (this->_capacity == 0)
		{
			this->_capacity = 1 * _multiplicativeCoef;
			Value* newData = new Value[this->_capacity];
			newData[this->_size] = value;
			this->_size += 1;
			this->_data = newData;
		}
		else
		{
			this->_capacity *= this->_multiplicativeCoef;
			Value* newData = new Value[this->_capacity];
			for ( size_t i = 1; i < this->_size; ++i )
			{
				newData[i] = this->_data[i - 1];
			}
			newData[0] = value;
			this->_size += 1;
			this->_data = newData;
		}
	}
	else
	{
		for ( size_t i = this->_size; i > 0; --i )
		{
			this->_data[i] = this->_data[i - 1];
		}
		this->_data[0] = value;
		this->_size += 1;
	}
}

void Vector::insert(const Value& value, size_t pos)
{
	if ( (pos < _size) && (pos >= 0) )
	{
		this->_capacity += 1;
		this->_size += 1;
		Value* newData = new Value[this->_capacity];
		for ( size_t i = 0; i < pos; ++i )
		{
			newData[i] = this->_data[i];
		}
		newData[pos] = value;
		for (size_t i = pos + 1; i < this->_size; ++i)
		{
			newData[i] = this->_data[i - 1];
		}
		this->_data = newData;
	}
}

void Vector::insert(const Value* values, size_t size, size_t pos)
{
	if ( (pos <= this->_size) && (pos >= 0) )
	{
		this->_capacity += size;
		this->_size += size;
		Value* newData = new Value[this->_capacity];
		for ( size_t i = 0; i < pos; ++i )
		{
			newData[i] = this->_data[i];
		}
		for ( size_t i = 0; i < size; ++i )
		{
			newData[i + pos] = values[i];
		}
		for ( size_t i = pos + size; i < this->_size; ++i )
		{
			newData[i] = this->_data[i - size];
		}
		this->_data = newData;
	}
}

void Vector::insert(const Vector& vector, size_t pos)
{
	if ( (pos <= this->_size) && (pos >= 0) )
	{
		this->_capacity += vector.size();
		this->_size += vector.size();
		Value* newData = new Value[this->_capacity];
		for ( size_t i = 0; i < pos; ++i )
		{
			newData[i] = this->_data[i];
		}
		for ( size_t i = 0; i < vector.size(); ++i )
		{
			newData[i + pos] = vector[i];
		}
		for ( size_t i = pos + vector.size(); i < this->_size; ++i )
		{
			newData[i] = this->_data[i - vector.size()];
		}
		this->_data = newData;
	}
}
	
void Vector::erase(size_t pos, size_t count)
{
	size_t k = 0;
	if ( (pos < this->_size) && (pos >= 0) )
	{
		if ( pos + count < this->_size)
		{
			while ( k != count )
			{
				for ( size_t i = pos; i < this->_size; ++i )
				{
					this->_data[i] = this->_data[i + 1];
				}
				++k;
			}
			for ( size_t i = 0; i < count; ++i )
			{
				this->popBack();
			}
		}
		else
		{
			size_t size = this->_size;
			for ( size_t i = pos; i < size; ++i )
			{
				this->popBack();
			}
		}
	}
}

void Vector::eraseBetween(size_t beginPos, size_t endPos)
{
	size_t k = 0;
	if ( (beginPos < this->_size) && (beginPos >= 0) )
	{
		if ( endPos < this->_size )
		{
			while ( k != endPos - beginPos )
			{
				for ( size_t i = beginPos; i < _size; ++i )
				{
					this->_data[i] = this->_data[i + 1];
				}
				++k;
			}
			for ( size_t i = 0; i < endPos - beginPos; ++i )
			{
				this->popBack();
			}
		}
		else
		{
			size_t size = this->_size - beginPos;
			for ( size_t i = 0; i < size; ++i )
			{
				this->popBack();
			}	
		}
	}
}

Vector::Iterator::Iterator(Value* ptr)
{
	this->_ptr = ptr;
}

Value& Vector::Iterator::operator*()
{
	return *this->_ptr;
}

 const Value& Vector::Iterator::operator*() const
{
	return *this->_ptr;
}

Value* Vector::Iterator::operator->()
{
	return this->_ptr;
}

 const Value* Vector::Iterator::operator->() const
{
	return this->_ptr;
}

Vector::Iterator Vector::Iterator::operator++()
{
	++this->_ptr;
	return *this;
}

Vector::Iterator Vector::Iterator::operator++(int)
{
	Iterator itr(*this);
	++this->_ptr;
	return itr;
}

bool Vector::Iterator::operator==(const Vector::Iterator& other) const
{
	return this->_ptr == other._ptr;
}

bool Vector::Iterator::operator!=(const Vector::Iterator& other) const
{
	return this->_ptr != other._ptr;
}

Vector::Iterator Vector::begin()
{
	return Iterator(this->_data);
}

Vector::Iterator Vector::end()
{
	return Iterator(this->_data + this->_size);
}
