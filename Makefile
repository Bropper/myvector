TARGET = Vector
.PHONY: all clean run
all: $(TARGET)
clean:
   rm $(TARGET) *.o
run: $(TARGET)
   ./$(TARGET)
main.o: main.cpp
   g++ -c main.cpp
Vector.o: Vector.cpp
   g++ -c Vector.cpp
$(TARGET): main.o Vector.o
   g++ main.o Vector.o -o $(TARGET)
