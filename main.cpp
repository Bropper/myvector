#include <iostream>
#include "Vector.h"

int main()
{
	int size = 5;
	double* mass = new double[size];
	for (int i = 0; i < size; ++i)
	{
		mass[i] = 0;
	}
	Vector v(mass, size);
	int size1 = 5;
	double* mass1 = new double[size1];
	for (int i = 0; i < size1; ++i)
	{
		mass1[i] = i + 1;
	}
	Vector v1(mass1, size1);
	v.insert(v1, 0);
	for (int i = 0; i < v.size(); ++i)
	{
		std::cout << v[i] << " ";
	}
	return 0;
}
